/*!
 * 
 * 
 * 
 * @author Thuclfc
 * @version 
 * Copyright 2021. MIT licensed.
 */"use strict";

$(document).ready(function () {
  open_popup('.video', '.popup-video');
  $('.charcter__select li').on('click', function () {
    $('.charcter__select li').removeClass('active');
    $(this).addClass('active');
    var sect_img = $(this).data('img');
    var sect_video = $(this).data('video');
    var sect_class = $(this).data('class');
    var html_img = "<div class=\"charcter__img fadeIn animated ".concat(sect_class, "\"><img src=\"").concat(sect_img, "\" alt=\"\"></div>");
    var html_trailer = "<div class=\"charcter__video\"><iframe src=\"".concat(sect_video, "\" frameborder=\"0\" allow=\"autoplay; encrypted-media\" allowfullscreen></iframe></div>");
    $('.charcter__content').html('').append(html_img, html_trailer);
  });
  $(function () {
    $('#dg-container').gallery({
      autoplay: false
    });
  });
});