/*!
 * 
 * 
 * 
 * @author Thuclfc
 * @version 
 * Copyright 2021. MIT licensed.
 */"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

var Landing = /*#__PURE__*/function (_React$Component) {
  _inherits(Landing, _React$Component);

  var _super = _createSuper(Landing);

  function Landing(props) {
    _classCallCheck(this, Landing);

    return _super.call(this, props);
  }

  _createClass(Landing, [{
    key: "render",
    value: function render() {
      return /*#__PURE__*/React.createElement("div", null, /*#__PURE__*/React.createElement("main", {
        className: "pr"
      }, /*#__PURE__*/React.createElement("img", {
        src: './assets/img/landing/bg-landing.jpg',
        className: "bg",
        alt: ""
      }), /*#__PURE__*/React.createElement("div", {
        className: "main-inner"
      }, /*#__PURE__*/React.createElement("div", {
        className: "page page1"
      }, /*#__PURE__*/React.createElement("h1", null, /*#__PURE__*/React.createElement("a", {
        href: "",
        className: "logo"
      }, /*#__PURE__*/React.createElement("img", {
        src: './assets/img/landing/logo.png',
        alt: ""
      }))), /*#__PURE__*/React.createElement("a", {
        href: "javascript:;",
        title: "",
        className: "video",
        "data-link": ""
      }), /*#__PURE__*/React.createElement("div", {
        className: "diemdanh flex"
      }, /*#__PURE__*/React.createElement("div", {
        className: "diemdanh__btn"
      }, /*#__PURE__*/React.createElement("a", {
        href: "javascript:;",
        title: "\u0110\u0103ng nh\u1EADp",
        className: "btn_login"
      }, "\u0110\u0103ng nh\u1EADp"), /*#__PURE__*/React.createElement("button", {
        type: "button",
        className: "btn_rule notclick",
        "data-toggle": "modal",
        "data-target": "#popup-rule"
      }, "Th\u1EC3 l\u1EC7")), /*#__PURE__*/React.createElement("ul", {
        className: "diemdanh__list flex"
      }, /*#__PURE__*/React.createElement("li", {
        className: "active"
      }, /*#__PURE__*/React.createElement("div", {
        className: "img"
      }, /*#__PURE__*/React.createElement("img", {
        src: './assets/img/landing/item1.png',
        alt: ""
      })), /*#__PURE__*/React.createElement("span", null, "Ng\xE0y 1"), /*#__PURE__*/React.createElement("a", {
        href: "javascript:;",
        title: ""
      }, "\u0110\xE3 nh\u1EADn")), /*#__PURE__*/React.createElement("li", {
        className: "active"
      }, /*#__PURE__*/React.createElement("div", {
        className: "img"
      }, /*#__PURE__*/React.createElement("img", {
        src: './assets/img/landing/item2.png',
        alt: ""
      })), /*#__PURE__*/React.createElement("span", null, "Ng\xE0y 2"), /*#__PURE__*/React.createElement("a", {
        href: "javascript:;",
        title: ""
      }, "Nh\u1EADn qu\xE0")), /*#__PURE__*/React.createElement("li", null, /*#__PURE__*/React.createElement("div", {
        className: "img"
      }, /*#__PURE__*/React.createElement("img", {
        src: './assets/img/landing/item3.png',
        alt: ""
      })), /*#__PURE__*/React.createElement("span", null, "Ng\xE0y 3"), /*#__PURE__*/React.createElement("a", {
        href: "javascript:;",
        title: ""
      }, "Nh\u1EADn qu\xE0")), /*#__PURE__*/React.createElement("li", null, /*#__PURE__*/React.createElement("div", {
        className: "img"
      }, /*#__PURE__*/React.createElement("img", {
        src: './assets/img/landing/item4.png',
        alt: ""
      })), /*#__PURE__*/React.createElement("span", null, "Ng\xE0y 4"), /*#__PURE__*/React.createElement("a", {
        href: "javascript:;",
        title: ""
      }, "Nh\u1EADn qu\xE0")), /*#__PURE__*/React.createElement("li", null, /*#__PURE__*/React.createElement("div", {
        className: "img"
      }, /*#__PURE__*/React.createElement("img", {
        src: './assets/img/landing/item5.png',
        alt: ""
      })), /*#__PURE__*/React.createElement("span", null, "Ng\xE0y 5"), /*#__PURE__*/React.createElement("a", {
        href: "javascript:;",
        title: ""
      }, "Nh\u1EADn qu\xE0")), /*#__PURE__*/React.createElement("li", null, /*#__PURE__*/React.createElement("div", {
        className: "img"
      }, /*#__PURE__*/React.createElement("img", {
        src: './assets/img/landing/item6.png',
        alt: ""
      })), /*#__PURE__*/React.createElement("span", null, "Ng\xE0y 6"), /*#__PURE__*/React.createElement("a", {
        href: "javascript:;",
        title: ""
      }, "Nh\u1EADn qu\xE0")), /*#__PURE__*/React.createElement("li", null, /*#__PURE__*/React.createElement("div", {
        className: "img"
      }, /*#__PURE__*/React.createElement("img", {
        src: './assets/img/landing/item7.png',
        alt: ""
      })), /*#__PURE__*/React.createElement("span", null, "Ng\xE0y 7"), /*#__PURE__*/React.createElement("a", {
        href: "javascript:;",
        title: ""
      }, "Nh\u1EADn qu\xE0"))))), /*#__PURE__*/React.createElement("div", {
        className: "page page2 pr"
      }, /*#__PURE__*/React.createElement("div", {
        className: "wheel pr"
      }, /*#__PURE__*/React.createElement("a", {
        href: "javascript:;",
        className: "spin_button",
        id: "spin_button"
      }), /*#__PURE__*/React.createElement("canvas", {
        id: "canvas",
        width: "548",
        height: "548",
        "data-responsiveMinWidth": "180",
        "data-responsiveScaleHeight": "true"
      }, /*#__PURE__*/React.createElement("p", {
        align: "center"
      }, "Sorry, your browser doesn't support canvas. Please try another.")), /*#__PURE__*/React.createElement("ul", {
        className: "wheel__btn flex"
      }, /*#__PURE__*/React.createElement("li", null, /*#__PURE__*/React.createElement("a", {
        href: "javascript:;",
        className: "btn_bxh",
        "data-toggle": "modal",
        "data-target": "#popup-rule"
      }, "Th\u1EC3 l\u1EC7")), /*#__PURE__*/React.createElement("li", null, /*#__PURE__*/React.createElement("a", {
        href: "javascript:;",
        title: "Ph\u1EA7n th\u01B0\u1EDFng",
        className: "btn_gift"
      }, "Ph\u1EA7n th\u01B0\u1EDFng")), /*#__PURE__*/React.createElement("li", null, /*#__PURE__*/React.createElement("span", null, "L\u01B0\u1EE3t quay: 08")), /*#__PURE__*/React.createElement("li", null, /*#__PURE__*/React.createElement("a", {
        href: "",
        title: "Th\xEAm l\u01B0\u1EE3t",
        className: "btn_gift"
      }, "Th\xEAm l\u01B0\u1EE3t"))))), /*#__PURE__*/React.createElement("div", {
        className: "page page3 pr"
      }, /*#__PURE__*/React.createElement("div", {
        className: "charcter"
      }, /*#__PURE__*/React.createElement("ul", {
        className: "charcter__select flex"
      }, /*#__PURE__*/React.createElement("li", {
        className: "active",
        "data-class": "charcter__img--nhatnguyet",
        "data-img": './assets/img/landing/nhatnguyet.png',
        "data-link": "a"
      }, /*#__PURE__*/React.createElement("i", {
        className: "icon icon-1"
      }), /*#__PURE__*/React.createElement("span", null, "Nh\u1EADt nguy\u1EC7t")), /*#__PURE__*/React.createElement("li", {
        "data-class": "charcter__img--daohoa",
        "data-img": './assets/img/landing/daohoa.png',
        "data-link": "b"
      }, /*#__PURE__*/React.createElement("i", {
        className: "icon icon-2"
      }), /*#__PURE__*/React.createElement("span", null, "\u0110\xE0o hoa")), /*#__PURE__*/React.createElement("li", {
        "data-class": "charcter__img--ngudoc",
        "data-img": './assets/img/landing/ngudoc.png',
        "data-link": "a"
      }, /*#__PURE__*/React.createElement("i", {
        className: "icon icon-3"
      }), /*#__PURE__*/React.createElement("span", null, "Ng\u0169 \u0110\u1ED9c")), /*#__PURE__*/React.createElement("li", {
        "data-class": "charcter__img--tangkiem",
        "data-img": './assets/img/landing/tangkiem.png',
        "data-link": "a"
      }, /*#__PURE__*/React.createElement("i", {
        className: "icon icon-4"
      }), /*#__PURE__*/React.createElement("span", null, "T\xE0ng Ki\u1EBFm"))), /*#__PURE__*/React.createElement("div", {
        className: "charcter__content"
      }, /*#__PURE__*/React.createElement("div", {
        className: "charcter__video"
      }, /*#__PURE__*/React.createElement("iframe", {
        src: "",
        frameBorder: "0",
        allow: "autoplay; encrypted-media",
        allowFullScreen: true
      })), /*#__PURE__*/React.createElement("div", {
        className: "charcter__img charcter__img--nhatnguyet"
      }, /*#__PURE__*/React.createElement("img", {
        src: './assets/img/landing/nhatnguyet.png',
        alt: ""
      }))))), /*#__PURE__*/React.createElement("div", {
        className: "page page4 pr"
      }, /*#__PURE__*/React.createElement("div", {
        className: "box_slider pr"
      }, /*#__PURE__*/React.createElement("div", {
        id: "dg-container",
        className: "dg-container"
      }, /*#__PURE__*/React.createElement("div", {
        className: "dg-wrapper"
      }, /*#__PURE__*/React.createElement("div", {
        className: "item"
      }, /*#__PURE__*/React.createElement("img", {
        src: './assets/img/landing/slide.jpg',
        alt: ""
      })), /*#__PURE__*/React.createElement("div", {
        className: "item"
      }, /*#__PURE__*/React.createElement("img", {
        src: './assets/img/landing/slide.jpg',
        alt: ""
      })), /*#__PURE__*/React.createElement("div", {
        className: "item"
      }, /*#__PURE__*/React.createElement("img", {
        src: './assets/img/landing/slide.jpg',
        alt: ""
      })), /*#__PURE__*/React.createElement("div", {
        className: "item"
      }, /*#__PURE__*/React.createElement("img", {
        src: './assets/img/landing/slide.jpg',
        alt: ""
      })), /*#__PURE__*/React.createElement("div", {
        className: "item"
      }, /*#__PURE__*/React.createElement("img", {
        src: './assets/img/landing/slide.jpg',
        alt: ""
      }))), /*#__PURE__*/React.createElement("nav", null, /*#__PURE__*/React.createElement("span", {
        className: "dg-prev"
      }), /*#__PURE__*/React.createElement("span", {
        className: "dg-next"
      }))))), /*#__PURE__*/React.createElement("div", {
        className: "modal fade popup-rule",
        id: "popup-rule"
      }, /*#__PURE__*/React.createElement("div", {
        className: "modal-dialog",
        role: "document"
      }, /*#__PURE__*/React.createElement("div", {
        className: "modal-content"
      }, /*#__PURE__*/React.createElement("a", {
        href: "javascript:;",
        className: "close",
        "data-dismiss": "modal"
      }), /*#__PURE__*/React.createElement("h5", {
        className: "modal-title"
      }, "Th\u1EC3 l\u1EC7"), /*#__PURE__*/React.createElement("article", null, "the le")))), /*#__PURE__*/React.createElement("div", {
        className: "modal fade popup-vq",
        id: "popup-vq"
      }, /*#__PURE__*/React.createElement("div", {
        className: "modal-dialog",
        role: "document"
      }, /*#__PURE__*/React.createElement("div", {
        className: "modal-content"
      }, /*#__PURE__*/React.createElement("a", {
        href: "javascript:;",
        className: "close",
        "data-dismiss": "modal"
      }), /*#__PURE__*/React.createElement("div", {
        className: "result-vq"
      }, /*#__PURE__*/React.createElement("h4", null, "Ch\xFAc M\u1EEBng B\u1EA1n \u0110\xE3 Quay \u0110\u01B0\u1EE3c Ph\u1EA7n Th\u01B0\u1EDFng"), /*#__PURE__*/React.createElement("div", {
        className: "img"
      }, /*#__PURE__*/React.createElement("img", {
        src: './assets/img/landing/iphone.png',
        alt: ""
      })), /*#__PURE__*/React.createElement("p", null, "IPHONE 12 PROMAX"))))))), /*#__PURE__*/React.createElement("footer", {
        className: "footer"
      }, /*#__PURE__*/React.createElement("div", {
        className: "fixCen"
      }, /*#__PURE__*/React.createElement("div", {
        className: "inner-ft pRel textAc inner pr",
        itemScope: "",
        itemType: "http://schema.org/Organization"
      }, /*#__PURE__*/React.createElement("a", {
        href: "http://sohagame.vn",
        title: "SohaGame",
        className: "shg pAbs",
        target: "_blank"
      }), /*#__PURE__*/React.createElement("a", {
        href: "javascript:void(0)",
        title: "",
        className: "logo-dt"
      }, /*#__PURE__*/React.createElement("span", {
        itemProp: "legalName"
      })), /*#__PURE__*/React.createElement("a", {
        className: "game_name",
        itemProp: "url",
        href: "",
        title: "T\xEAn Game"
      }, /*#__PURE__*/React.createElement("span", {
        itemProp: "name"
      }, "T\xEAn Game")), " Game sologan", /*#__PURE__*/React.createElement("br", null), "Ph\xE1t h\xE0nh b\u1EDFi ", /*#__PURE__*/React.createElement("a", {
        href: "http://sohagame.vn/",
        title: "Game",
        itemProp: "url"
      }, /*#__PURE__*/React.createElement("span", {
        itemProp: "legalName"
      }, "SohaGame")), /*#__PURE__*/React.createElement("br", null), /*#__PURE__*/React.createElement("a", {
        href: "http://sohagame.vn",
        title: "SohaGame",
        target: "_blank"
      }, "SohaGame - C\u1ED9ng \u0111\u1ED3ng game mobile \u0111\xF4ng vui nh\u1EA5t Vi\u1EC7t Nam"), " ", /*#__PURE__*/React.createElement("br", null), /*#__PURE__*/React.createElement("span", {
        className: "az"
      }, "Hotline"), ":\xA019006639 -\xA0", /*#__PURE__*/React.createElement("span", {
        className: "az"
      }, "Email"), ":\xA0", /*#__PURE__*/React.createElement("a", {
        href: "mailto:hotrogame@sohagame.vn",
        title: "hotrogame@sohagame.vn"
      }, "hotro@sohagame.vn"), /*#__PURE__*/React.createElement("br", null), /*#__PURE__*/React.createElement("span", null, "H\u1ED7 tr\u1EE3 qu\u1ED1c t\u1EBF: (+84) 24-73-09 5555 - Ext: 950"), /*#__PURE__*/React.createElement("div", {
        className: "ticket-link"
      }, "B\xE1o l\u1ED7i: ", /*#__PURE__*/React.createElement("a", {
        href: "http://taikhoan.sohagame.vn/ticket"
      }, "http://taikhoan.sohagame.vn/ticket")), /*#__PURE__*/React.createElement("div", null, "C\xF4ng ty c\u1ED5 ph\u1EA7n VCCorp - ", /*#__PURE__*/React.createElement("span", {
        className: "az"
      }, "\u0110\u1ECBa ch\u1EC9"), ": T\u1EA7ng 17,19,20,21 T\xF2a nh\xE0 Hapulico Center, s\u1ED1 1 Nguy\u1EC5n Huy T\u01B0\u1EDFng, H\xE0 N\u1ED9i."), /*#__PURE__*/React.createElement("a", {
        href: "http://sohagame.vn/dieu-khoan",
        target: "_blank",
        title: "\u0110i\u1EC1u kho\u1EA3n"
      }, "\u0110i\u1EC1u kho\u1EA3n"), " - ", /*#__PURE__*/React.createElement("a", {
        href: "",
        title: "H\u01B0\u1EDBng d\u1EABn c\xE0i \u0111\u1EB7t v\xE0 g\u1EE1 b\u1ECF"
      }, "H\u01B0\u1EDBng d\u1EABn c\xE0i \u0111\u1EB7t v\xE0 g\u1EE1 b\u1ECF"), /*#__PURE__*/React.createElement("a", {
        href: "/kich-ban-huong-dan",
        className: "kich-ban",
        style: {
          display: "none"
        }
      }, " - K\u1ECBch b\u1EA3n h\u01B0\u1EDBng d\u1EABn"), /*#__PURE__*/React.createElement("a", {
        href: "/kich-ban-huong-dan",
        className: "kich-ban"
      }, " - K\u1ECBch b\u1EA3n h\u01B0\u1EDBng d\u1EABn"), /*#__PURE__*/React.createElement("p", {
        className: "rs"
      }, "Ch\u01A1i qu\xE1 180 ph\xFAt m\u1ED9t ng\xE0y s\u1EBD \u1EA3nh h\u01B0\u1EDFng x\u1EA5u \u0111\u1EBFn s\u1EE9c kh\u1ECFe"), /*#__PURE__*/React.createElement("img", {
        src: "https://sohagame.vcmedia.vn/public/sg73/nut-60px.jpg",
        className: "pAbs img-teen limit",
        width: "60",
        height: "98",
        alt: ""
      }), /*#__PURE__*/React.createElement("a", {
        className: "backtops",
        href: "javascript:void(0)"
      }), /*#__PURE__*/React.createElement("div", {
        className: "logos"
      }, /*#__PURE__*/React.createElement("a", {
        href: "",
        className: "logo",
        title: ""
      }), /*#__PURE__*/React.createElement("a", {
        href: "https://sohagame.vn",
        className: "soha",
        title: "SohaGame"
      }), /*#__PURE__*/React.createElement("div", {
        className: "otherLogo"
      })))), /*#__PURE__*/React.createElement("img", {
        style: {
          position: "fixed",
          top: "40px",
          left: "0",
          zIndex: "9"
        },
        className: "logo_limmit",
        src: "https://sohagame.vcmedia.vn/public/sg148/soha-game-dong-ta-tay-doc-logo.png",
        width: "150",
        height: "auto",
        alt: ""
      })));
    }
  }]);

  return Landing;
}(React.Component);

ReactDOM.render( /*#__PURE__*/React.createElement(Landing, null), document.getElementById('landing'));