

class Landing extends React.Component{
    constructor(props){
        super(props);
    }
    render() {
        return(
            <div>
                <main className="pr">
                    <img src={'./assets/img/landing/bg-landing.jpg'} className="bg" alt="" />
                    <div className="main-inner">
                        <div className="page page1">
                            <h1><a href="" className="logo"><img src={'./assets/img/landing/logo.png'} alt="" /></a></h1>
                            <a href="javascript:;" title="" className="video" data-link=""></a>
                            <div className="diemdanh flex">
                                <div className="diemdanh__btn">
                                    <a href="javascript:;" title="Đăng nhập" className="btn_login">Đăng nhập</a>
                                    <button type="button" className="btn_rule notclick" data-toggle="modal"
                                            data-target="#popup-rule">Thể lệ</button>
                                </div>
                                <ul className="diemdanh__list flex">
                                    <li className="active">
                                        <div className="img"><img src={'./assets/img/landing/item1.png'} alt="" /></div>
                                        <span>Ngày 1</span>
                                        <a href="javascript:;" title="">Đã nhận</a>
                                    </li>
                                    <li className="active">
                                        <div className="img"><img src={'./assets/img/landing/item2.png'} alt="" /></div>
                                        <span>Ngày 2</span>
                                        <a href="javascript:;" title="">Nhận quà</a>
                                    </li>
                                    <li>
                                        <div className="img"><img src={'./assets/img/landing/item3.png'} alt="" /></div>
                                        <span>Ngày 3</span>
                                        <a href="javascript:;" title="">Nhận quà</a>
                                    </li>
                                    <li>
                                        <div className="img"><img src={'./assets/img/landing/item4.png'} alt="" /></div>
                                        <span>Ngày 4</span>
                                        <a href="javascript:;" title="">Nhận quà</a>
                                    </li>
                                    <li>
                                        <div className="img"><img src={'./assets/img/landing/item5.png'} alt="" /></div>
                                        <span>Ngày 5</span>
                                        <a href="javascript:;" title="">Nhận quà</a>
                                    </li>
                                    <li>
                                        <div className="img"><img src={'./assets/img/landing/item6.png'} alt="" /></div>
                                        <span>Ngày 6</span>
                                        <a href="javascript:;" title="">Nhận quà</a>
                                    </li>
                                    <li>
                                        <div className="img"><img src={'./assets/img/landing/item7.png'} alt="" /></div>
                                        <span>Ngày 7</span>
                                        <a href="javascript:;" title="">Nhận quà</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div className="page page2 pr">
                            <div className="wheel pr">
                                <a href="javascript:;" className="spin_button" id="spin_button"></a>
                                <canvas id="canvas" width="548" height="548" data-responsiveMinWidth="180"
                                        data-responsiveScaleHeight="true">
                                    <p align="center">Sorry, your browser doesn't support canvas.
                                        Please try another.</p>
                                </canvas>
                                <ul className="wheel__btn flex">
                                    <li><a href="javascript:;" className="btn_bxh" data-toggle="modal"
                                           data-target="#popup-rule">Thể lệ</a></li>
                                    <li><a href="javascript:;" title="Phần thưởng" className="btn_gift">Phần thưởng</a>
                                    </li>
                                    <li><span>Lượt quay: 08</span></li>
                                    <li><a href="" title="Thêm lượt" className="btn_gift">Thêm lượt</a></li>
                                </ul>
                            </div>
                        </div>
                        <div className="page page3 pr">
                            <div className="charcter">
                                <ul className="charcter__select flex">
                                    <li className="active" data-class="charcter__img--nhatnguyet"
                                        data-img={ './assets/img/landing/nhatnguyet.png'} data-link="a">
                                        <i className="icon icon-1"></i>
                                        <span>Nhật nguyệt</span>
                                    </li>
                                    <li data-class="charcter__img--daohoa" data-img={'./assets/img/landing/daohoa.png'}
                                        data-link="b">
                                        <i className="icon icon-2"></i>
                                        <span>Đào hoa</span>
                                    </li>
                                    <li data-class="charcter__img--ngudoc" data-img={'./assets/img/landing/ngudoc.png'}
                                        data-link="a">
                                        <i className="icon icon-3"></i>
                                        <span>Ngũ Độc</span>
                                    </li>
                                    <li data-class="charcter__img--tangkiem"
                                        data-img={ './assets/img/landing/tangkiem.png'} data-link="a">
                                        <i className="icon icon-4"></i>
                                        <span>Tàng Kiếm</span>
                                    </li>
                                </ul>
                                <div className="charcter__content">
                                    <div className="charcter__video">
                                        <iframe src="" frameBorder="0" allow="autoplay; encrypted-media"
                                                allowFullScreen></iframe>
                                    </div>
                                    <div className="charcter__img charcter__img--nhatnguyet">
                                        <img src={ './assets/img/landing/nhatnguyet.png'} alt="" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="page page4 pr">
                            <div className="box_slider pr">
                                <div id="dg-container" className="dg-container">
                                    <div className="dg-wrapper">
                                        <div className="item"><img src={'./assets/img/landing/slide.jpg'} alt="" /></div>
                                        <div className="item"><img src={'./assets/img/landing/slide.jpg'} alt="" /></div>
                                        <div className="item"><img src={'./assets/img/landing/slide.jpg'} alt="" /></div>
                                        <div className="item"><img src={'./assets/img/landing/slide.jpg'} alt="" /></div>
                                        <div className="item"><img src={'./assets/img/landing/slide.jpg'} alt="" /></div>
                                    </div>
                                    <nav>
                                        <span className="dg-prev"></span>
                                        <span className="dg-next"></span>
                                    </nav>
                                </div>
                            </div>
                        </div>
                        <div className="modal fade popup-rule" id="popup-rule">
                            <div className="modal-dialog" role="document">
                                <div className="modal-content">
                                    <a href="javascript:;" className="close" data-dismiss="modal"></a>
                                    <h5 className="modal-title">Thể lệ</h5>
                                    <article>
                                        the le
                                    </article>
                                </div>
                            </div>
                        </div>
                        <div className="modal fade popup-vq" id="popup-vq">
                            <div className="modal-dialog" role="document">
                                <div className="modal-content">
                                    <a href="javascript:;" className="close" data-dismiss="modal"></a>
                                    <div className="result-vq">
                                        <h4>Chúc Mừng Bạn Đã Quay Được Phần Thưởng</h4>
                                        <div className="img"><img src={ './assets/img/landing/iphone.png'} alt="" /></div>
                                        <p>IPHONE 12 PROMAX</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </main>
                <footer className="footer">
                    <div className="fixCen">
                        <div className="inner-ft pRel textAc inner pr" itemScope=""
                             itemType="http://schema.org/Organization">
                            <a href="http://sohagame.vn" title="SohaGame" className="shg pAbs" target="_blank"></a>
                            <a href="javascript:void(0)" title="" className="logo-dt"><span itemProp="legalName"></span></a>
                            <a className="game_name" itemProp="url" href="" title="Tên Game"><span itemProp="name">Tên Game</span>
                            </a> Game sologan<br />
                            Phát hành bởi <a href="http://sohagame.vn/" title="Game" itemProp="url"><span
                            itemProp="legalName">SohaGame</span></a><br />
                            <a href="http://sohagame.vn" title="SohaGame" target="_blank">SohaGame - Cộng đồng game
                                mobile đông vui
                                nhất Việt Nam</a> <br />
                            <span className="az">Hotline</span>:&nbsp;19006639 -&nbsp;<span
                            className="az">Email</span>:&nbsp;<a
                            href="mailto:hotrogame@sohagame.vn" title="hotrogame@sohagame.vn">hotro@sohagame.vn</a><br />
                            <span>Hỗ trợ quốc tế: (+84) 24-73-09 5555 - Ext: 950</span>
                            <div className="ticket-link">Báo lỗi: <a
                                href="http://taikhoan.sohagame.vn/ticket">http://taikhoan.sohagame.vn/ticket</a>
                            </div>
                            <div>
                                Công ty cổ phần VCCorp - <span className="az">Địa chỉ</span>: Tầng 17,19,20,21 Tòa nhà
                                Hapulico Center,
                                số 1 Nguyễn Huy Tưởng, Hà Nội.
                            </div>
                            <a href="http://sohagame.vn/dieu-khoan" target="_blank" title="Điều khoản">Điều
                                khoản</a> - <a
                            href="" title="Hướng dẫn cài đặt và gỡ bỏ">Hướng dẫn cài đặt và gỡ bỏ</a>

                            <a href="/kich-ban-huong-dan" className="kich-ban" style={{display: "none"}}> - Kịch bản hướng
                                dẫn</a>

                            <a href="/kich-ban-huong-dan" className="kich-ban"> - Kịch bản hướng dẫn</a>

                            <p className="rs">Chơi quá 180 phút một ngày sẽ ảnh hưởng xấu đến sức khỏe</p>
                            <img src="https://sohagame.vcmedia.vn/public/sg73/nut-60px.jpg"
                                 className="pAbs img-teen limit" width="60" height="98" alt=""/>
                            <a className="backtops" href="javascript:void(0)"></a>
                            <div className="logos">
                                <a href="" className="logo" title=""></a>
                                <a href="https://sohagame.vn" className="soha" title="SohaGame"></a>
                                <div className="otherLogo"></div>
                            </div>
                        </div>
                    </div>
                    <img style={{position: "fixed", top: "40px", left: "0", zIndex: "9"}} className="logo_limmit"
                         src="https://sohagame.vcmedia.vn/public/sg148/soha-game-dong-ta-tay-doc-logo.png" width="150"
                         height="auto" alt="" />
                </footer>
            </div>
        )
    }
}
ReactDOM.render(
    <Landing />,
    document.getElementById('landing')
)